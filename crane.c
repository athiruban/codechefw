#include <stdio.h>
#include <stdlib.h>

struct PACK {
  unsigned int N, H;
  int *boxes;
  bool crane_has_box;
  unsigned int current_crane_position;
};

void allocate_memory(PACK *pack, const int N, const int H) {
  int *boxes;
  boxes = (int *) malloc(sizeof(int) * N);
  pack->N = N; pack->H = H; pack->boxes = boxes;
}

void process_command(PACK *pack, unsigned int crane_cur_position, unsigned int user_command) {
  switch(user_command) {
  case 1:
    // move left
    if (pack->current_crane_position > 1) {
      pack->current_crane_position -= 1;
    }
    break;
  case 2:
    // move right
    if (pack->current_crane_position < N) {
      pack->current_crane_position += 1;
    }
    break;
  case 3:
    // pick up box
    if (pack->crane_has_box == false && pack->boxes[pack->current_crane_position - 1] > 0) {
      pack->crane_has_box = true;
      pack->boxes[pack->current_crane_position - 1] -= 1;
    }
    break;    
  case 4:
    // drop box
    if (pack->crane_has_box == true && pack->boxes[pack->current_crane_position - 1] < pack->H) {
      pack->crane_has_box = false;
      pack->boxes[pack->current_crane_position - 1] += 1;
    }
    break;
  case 0:
    // quit the program
    print_output(pack);
  }
}

int main(void) {
  return 0;
}